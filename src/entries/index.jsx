import React from 'react';
import ReactDOM from 'react-dom';
import App from '../containers/App';

const wrapper = document.querySelector('.root');
ReactDOM.render(<App />, wrapper);
